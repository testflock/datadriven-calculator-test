import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import utils.BaseTestCase;
import view.Scientific;
import view.Standard;

public class DriverScript extends BaseTestCase {

    WebDriver driver;
    Standard standard;
    Scientific scientific;

    @BeforeTest
    public void openCalculator() {
        driver = getChromeDriver();
        driver.get("http://www.testflock.org/calculator/");
    }

    @Test
    public void testCalculator() {
        standard = new Standard(driver);
        standard.testStandardView();

        scientific = new Scientific(driver);
        scientific.testScientificView();
    }

    @AfterTest
    public void closeCalculator() {
        driver.quit();
    }

}
