package view;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import utils.BaseWebPage;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.List;

public class Scientific extends BaseWebPage{

    public Scientific(WebDriver driver) {
        super(driver);
        initializeFields();
    }

    public void testScientificView() {
        checkScientificCalculator();

        String testDataPathString =
                Paths.get(".").toAbsolutePath().normalize().toString()
                        + "/src/main/resources/test-data/"
                        + this.getClass().getSimpleName()
                        + ".csv";

        File file = new File(testDataPathString);
        List<String> lines = null;
        try {
            lines = Files.readAllLines(file.toPath(),
                    StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (String line : lines) {
            String[] array = line.split(",");
            testCalculation(array);
            initializeCalculator();
        }

    }

    public void checkScientificCalculator() {
        String selectedView = driver.findElement(By.id("selectCalculator")).getAttribute("checked");

        if (selectedView != "true") {
            driver.findElement(By.id("switch")).click();
        }
    }

    public void initializeCalculator() {
        driver.findElement(By.id("reset-button")).click();        // click on CE
        wait(1);
    }

    public void testCalculation(String[] array) {
        DecimalFormat numberFormat = new DecimalFormat("#.##########");

        driver.findElement(By.id("number-" + array[0])).click();
        wait(1);

        if (array[2].equalsIgnoreCase("sin"))
            driver.findElement(By.id("sin-button")).click();              // click on 'sin'
        else if (array[2].equalsIgnoreCase("cos"))
            driver.findElement(By.id("cos-button")).click();              // click on 'cos'
        else if (array[2].equalsIgnoreCase("tan"))
            driver.findElement(By.id("tan-button")).click();              // click on 'tan'
        wait(1);

        if (!array[1].equalsIgnoreCase("")) {
            driver.findElement(By.id("number-" + array[1])).click();
            wait(1);
        }

        String actSin = driver.findElement(By.id("input-outputS")).getText().toString();
        double actDoubleSin = Double.parseDouble(actSin);
        String actualSin = numberFormat.format(actDoubleSin) + "";
        System.out.println("Actual "+array[2]+":"+ actualSin);

        double angleInDegree = 10;
        double angleInRadian = Math.toRadians(angleInDegree);
        double sin = Math.sin(angleInRadian);
        String expectedSin = numberFormat.format(sin) + "";
        System.out.println("Expected "+array[2]+":"+ expectedSin);

//        Assert.assertEquals(actualSin, expectedSin);
    }

}
