package view;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import utils.BaseWebPage;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Standard extends BaseWebPage {

    @FindBy(id = "selectCalculator")
    WebElement viewSelector;

    public Standard(WebDriver driver) {
        super(driver);
        initializeFields();
    }

    public void testStandardView() {
        checkStandardCalculator();

        String testDataPathString =
                Paths.get(".").toAbsolutePath().normalize().toString()
                        + "/src/main/resources/test-data/"
                        + this.getClass().getSimpleName()
                        + ".csv";

        File file = new File(testDataPathString);
        List<String> lines = null;
        try {
            lines = Files.readAllLines(file.toPath(),
                    StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (String line : lines) {
            String[] array = line.split(",");
            testCalculation(array);
            initializeCalculator();
        }

    }

    public void checkStandardCalculator() {
        String selectedView = viewSelector.getAttribute("checked");

        if (selectedView == "true") {
            driver.findElement(By.id("switch")).click();
        }

    }

    public void initializeCalculator() {
        driver.findElement(By.id("reset-buttonSC")).click();        // click on CE
        wait(1);
    }

    public void testCalculation(String[] array) {
        driver.findElement(By.id("number-" + array[0] + "SC")).click();
        wait(1);
        if (array[2].equalsIgnoreCase("+"))
            driver.findElement(By.id("add-buttonSC")).click();
        else if (array[2].equalsIgnoreCase("-"))
            driver.findElement(By.id("subtract-buttonSC")).click();
        else if (array[2].equalsIgnoreCase("/"))
            driver.findElement(By.id("division-button")).click();
        else if (array[2].equalsIgnoreCase("*"))
            driver.findElement(By.id("multiplication-buttonSC")).click();
        wait(1);
        driver.findElement(By.id("number-" + array[1] + "SC")).click();
        wait(1);
        driver.findElement(By.id("calculate-buttonSC")).click();
        wait(1);

        Assert.assertTrue(driver.findElement(By.id("input-output")).getText().toString().equals(array[3]));
    }


}
