# datadriven-calculator-test

Sample Calculator Testing Program implemented with Java, Selenium, TestNG with a Data-Driven Testing Framework.

demo page: http://www.testflock.org/calculator/

## Getting Started


## Authors

Vasilis Petrou | Automation Test Engineer
petrou82@gmail.com


## Acknowledgments

what is data-driven testing framework?

data driven testing is where the test input and expected output results are stored in a separate data file so that the single driver script can execute all the test cases with multiple sets of data.

the driver script contains:
 - navigation through the program
 - reading of the data files
 - logging of the test status informationes. 
